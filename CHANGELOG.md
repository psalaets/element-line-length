# Change Log

## [2.3.0] - 2023-03-15

### Changed

- Upgrade deps

## [2.2.2] - 2023-10-11

### Fixed

- Improve handling for lines that overlap vertically.

## [2.2.1] - 2023-10-11

### Fixed

- Better handling for lines containing text with differing heights.

## [2.2.0] - 2023-06-07

### Changed

- Small perf boost

## [2.1.0] - 2023-06-07

### Changed

- Upgrade deps

## [2.0.1] - 2023-05-18

### Added

- Metadata change to improve tool compat

## [2.0.0] - 2023-05-18

### Breaking

- Rename export `elementLineLengths` => `elementLineLength`

## [1.0.0] - 2023-05-18

Initial impl
