# element-line-length

Measure lines of text in a dom element.

## Install

```
npm install element-line-length
```

## Usage

```js
import { elementLineLength } from 'element-line-length';

const element = document.getElementById('my-article');
elementLineLength(element); // => [15, 17, 20, 19]
```

## Why?

Line length can affect readability and accessibility.

- https://baymard.com/blog/line-length-readability
- https://www.w3.org/WAI/tutorials/page-structure/styling/#line-length

## License

MIT

## Related

- [Bookmarklet](https://psalaets.github.io/line-length/)
- [Playwright plugin](https://github.com/psalaets/playwright-line-length)
