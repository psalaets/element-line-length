import { elementLineLength } from '../src/index';

[
  'narrow',
  'medium',
  'wide',
  'single',
  'whitespace-padding',
  'whitespace-internal',
  'empty',
  'entities',
  'breaks',
  'mixed',
  'different-heights',
  'overlapping',
  'empty-with-height',
]
  .map((id) => document.getElementById(id))
  .forEach((p) => {
    const form = document.createElement('form');

    // Create run button
    const runButton = document.createElement('button');
    runButton.id = `${p.id}-run`;
    runButton.textContent = 'Run';
    form.appendChild(runButton);

    // Create clear button
    const clearButton = document.createElement('button');
    clearButton.id = `${p.id}-clear`;
    clearButton.textContent = 'Clear';
    form.appendChild(clearButton);

    // Create output box
    const output = document.createElement('div');
    output.id = `${p.id}-output`;
    form.appendChild(output);

    // When run button clicked, check lengths
    runButton.addEventListener('click', (event) => {
      event.preventDefault();

      const lengths = elementLineLength(p);
      output.textContent = JSON.stringify(lengths);
    });

    // When clear button clicked, clear output
    clearButton.addEventListener('click', (event) => {
      event.preventDefault();

      output.textContent = '';
    });

    // Insert form into document
    p.insertAdjacentElement('afterend', form);
  });
