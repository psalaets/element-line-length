import { test, expect } from '@playwright/test';

const host = 'http://localhost:8080';

function makeTestCase(paragraphId, expected) {
  return async function testCase({ page }) {
    await page.goto(host);
    await page.locator(`#${paragraphId}-run`).click();
    await expect(page.locator(`#${paragraphId}-output`)).toHaveText(expected);
  };
}

test('narrow', makeTestCase('narrow', '[71,68,71,70,74,77,8]'));
test('medium', makeTestCase('medium', '[99,98,95,101,48]'));
test('wide', makeTestCase('wide', '[123,117,126,76]'));
test('single', makeTestCase('single', '[6]'));
test('whitespace-internal', makeTestCase('whitespace-internal', '[7]'));
test('whitespace-padding', makeTestCase('whitespace-padding', '[6]'));
test('empty', makeTestCase('empty', '[]'));
test('entities', makeTestCase('entities', '[15]'));
test('line breaks', makeTestCase('breaks', '[5,6,5]'));
test('mixed', makeTestCase('mixed', '[71,68,71,18]'));
test('different-heights', makeTestCase('different-heights', '[71,59,74,24]'));
test('overlapping', makeTestCase('overlapping', '[39,49,2]'));
test('empty-with-height', makeTestCase('empty-with-height', '[]'));

test('consecutive calls', async ({ page }) => {
  await page.goto(host);

  await page.locator(`#wide-run`).click();
  await expect(page.locator(`#wide-output`)).toHaveText('[123,117,126,76]');

  await page.locator(`#wide-clear`).click();

  await page.locator(`#wide-run`).click();
  await expect(page.locator(`#wide-output`)).toHaveText('[123,117,126,76]');
});
