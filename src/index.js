import { splitTextNodes } from 'split-text-nodes';

/**
 * @param {HTMLElement} element
 * @returns {Array<number>} Character count for each line of text in `element`
 */
export function elementLineLength(element) {
  // Put wrapper elements around words and whitespace
  const { wrappers, revert } = splitTextNodes(element);

  /**
   * @type {Array<Array<HTMLElement>>}
   */
  const elementsByLine = [];

  const currentLine = {
    maxHeight: 0,
    highestTop: -Infinity,
  };

  for (const wrapper of wrappers) {
    const { top, height } = wrapper.getBoundingClientRect();

    // Hack: If wrapper's top is at least 1/2 of current line's height below
    // the current line's top, consider it a new line of text.
    const isStartOfNewLine =
      top >= currentLine.highestTop + currentLine.maxHeight * 0.5;

    if (isStartOfNewLine) {
      // wrapper is on a new line
      elementsByLine.push([wrapper]);

      currentLine.highestTop = top;
      currentLine.maxHeight = height;
    } else {
      // wrapper is on the current line
      elementsByLine.at(-1).push(wrapper);

      currentLine.highestTop = Math.min(currentLine.highestTop, top);
      currentLine.maxHeight = Math.max(currentLine.maxHeight, height);
    }
  }

  const lengths = elementsByLine
    .map(
      (elements) =>
        elements
          .map((el) => el.innerText || '')
          .join('')
          .trim().length
    )
    // Discard any lines that had nothing - This can happen in safari when run
    // on an element consecutive times due to a leading text node of whitespace.
    .filter((length) => length);

  revert();

  return lengths;
}
